<?php

/**
 * @file
 * Contains \Drupal\graphs\Plugin\field\formatter\HighchartsFormatter.
 * See "http://jsfiddle.net/kkulig/rm0p5113/"
 */

namespace Drupal\graphs\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Highcharts' formatter.
 *
 * @FieldFormatter(
 *   id = "highcharts",
 *   label = @Translation("Highcharts"),
 *   field_types = {
 *     "integer"
 *   }
 * )
 */
class HighchartsFormatter extends FormatterBase implements ContainerFactoryPluginInterface {
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings']
    );
  }

  /**
   * Constructs a new SoftwareFormatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size' => '100%',
      'startAngle' => -90,
      'endAngle' => 90,
      'backgroundColor' => '#EEE',
      'innerRadius' => '60%',
      'outerRadius' => '100%',
      'shape' => 'arc',
      'min' => 0,
      'max' => 100,
      'stopsPoint' => '0.1, 0.5, 0.9',
      'stopsColors' => '#55BF3B, #DDDF0D, #DF5353',
      'ranges' => '4,10,20,100',
      'rangesLabels' => 'Below average,Average,Above average,Significantly above average',
      'reversed' => false,
      'subLabel' => 'SubLabel',
      'tooltip' => '',
      'anonymousView' => false,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $elements = parent::settingsForm($form, $form_state);
    // Pane Settings.
    $text_attributes = array (
      'size',
      'startAngle',
      'endAngle',
      'backgroundColor',
      'innerRadius',
      'outerRadius',
      'shape',
      'min',
      'max',
      'stopsPoint',
      'stopsColors',
      'ranges',
      'rangesLabels',
      'subLabel',
      'tooltip',
    );
    foreach ($text_attributes as $attribute) {
      $elements[$attribute] = array(
        '#type' => 'textfield',
        '#title' => t($attribute),
        '#default_value' => $this->getSetting($attribute),
      );
    }

    $radio_attributes = array (
      'reversed',
      'anonymousView',
    );
    foreach ($radio_attributes as $attribute) {
      $elements[$attribute] = array(
        '#type' => 'checkbox',
        '#title' => t($attribute),
        '#default_value' => $this->getSetting($attribute),
      );
    }

    $text_area_attributes = array (
      'tooltip',
    );
    foreach ($text_area_attributes as $attribute) {
      $elements[$attribute] = array(
        '#type' => 'textarea',
        '#title' => t($attribute),
        '#default_value' => $this->getSetting($attribute),
      );
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL) {
    $elements = [];
    $field_name = $this->fieldDefinition->getItemDefinition()->getFieldDefinition()->getName();

    $stops = explode(",", $this->getSetting('stopsPoint'));
    $ranges = explode(",", $this->getSetting('ranges'));
    $ranges_labels = explode(",", $this->getSetting('rangesLabels'));
    $subLabel = $this->getSetting('subLabel');
    if ($this->getSetting('anonymousView')) {
      $elements[0] = array(
        '#text' => '<p class="text-center">' . $subLabel . '</p><div class="outer highcharts-fieldformatter"><div id="' . $field_name . '_container" class="container"></div></div><p class="text-center">####</p>' . $this->buildGraph($field_name, $fallback_vale) ,
        '#type' => 'processed_text',
        '#format' => 'html',
      );
    }
    else {
      if ($items->isEmpty()) {
        $fallback_vale = $this->getSetting('reversed') ? 100 : 0;
        $elements[0] = array(
          '#text' => '<p class="text-center">' . $subLabel . '</p><div class="outer highcharts-fieldformatter"><div id="' . $field_name . '_container" class="container"></div></div><p class="text-center">n/a</p>' . $this->buildGraph($field_name, $fallback_vale) ,
          '#type' => 'processed_text',
          '#format' => 'html',
        );
      }
      else {
        foreach ($items as $delta => $item) {

          // Round of the value to stop point as per range.
          // E.g. we have ranges 4, 10, 20, 100
          // and stop points 25, 50, 75, 100
          // If value is 3.9,  chartValue is 25
          // If value is 4.1,  chartValue is 50
          // If value is 9.9,  chartValue is 50
          // If value is 10.1, chartValue is 75
          // If value is 10.9, chartValue is 75
          // If value is 20.1, chartValue is 100
          // If value above 20,chartValue is 100
          foreach ($ranges as $key => $value) {
            if ($item->value <= $value) {
              $chartValue = $stops[$key];
              $chartLabel = isset($ranges_labels) ? $ranges_labels[$key] : " ";
              break;
            }
            else {
              $chartValue = $stops[$key];
              $chartLabel = isset($ranges_labels) ? $ranges_labels[$key] : " ";
            }
          }
          $output = '<p class="text-center">' . $subLabel . '</p><div class="outer highcharts-fieldformatter"><div id="' . $field_name . '_container" class="container"></div></div>';
          if ($this->getSetting('tooltip') != "") {
            $output .= '<a href="#" data-toggle="tooltip" title="' . $this->getSetting('tooltip') . '" class="information-icon" data-placement="top"><em>i</em></a>';
          }
          $output .= '<p class="text-center">' . $chartLabel . '</p>';
          $output .= $this->buildGraph($field_name, $chartValue);
          $elements[$delta] = array(
            '#text' => $output,
            '#type' => 'processed_text',
            '#format' => 'html',
          );
        }
      }
    }


    return $elements;
  }

  /**
   * Helper function to build graph js.
   */
  private function buildGraph($elementName, $chartValue) {
    if ($this->getSetting('reversed')) {
      $computePoints = "function computePoints(val) {
        var data = [];
        var i = 0;
        data.unshift({
          y: val,
          color: stopColors[i],
        });
        return data;
      }";
    }
    else {
      $computePoints = "function computePoints(val) {
        var data = [];
        var i = 0;
        stopsPoint.forEach(function(stop) {
          if (val > stop) {
            data.unshift({
              y: stop,
              color: stopColors[i],
            });
            i++;
          }
        });
        data.unshift({
          y: val,
          color: stopColors[i],
        });
        return data;
      }";
    }


    $chart = "
    <script type='text/javascript'>
      (function ($) {
        var stopsPoint = [" . $this->getSetting('stopsPoint') . "];
        var stopColors = [" . $this->getSetting('stopsColors') . "];
        var gaugeOptions = {
          chart: {
            type: 'solidgauge',
            backgroundColor: 'transparent',
          },
          title: null,
          pane: {
            center: ['50%', '95%'],
            size: '190%',
            startAngle: -90,
            endAngle: 90,
            background: {
              backgroundColor: '#EEE',
              innerRadius: '60%',
              outerRadius: '100%',
              shape: 'arc',
              borderColor: 'transparent'
            }
          },
          tooltip: {
            enabled: false
          },
          yAxis: {
            lineWidth: 0,
            minorTickInterval: null,
            tickLength: 30,
            tickWidth: 2,
            tickInterval: 8,
            tickColor: '#555',
            zIndex: 6,
            labels: {
              enabled: false
            },
            gridLineWidth: 0,
            gridLineColor: 'transparent',
            min: " . $this->getSetting('min') . ",
            max: " . $this->getSetting('max') . ",
            reversed: " . (($this->getSetting('reversed')) ? 'true' : 'false') . ",
          },
          plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
          },
          credits: {
            enabled: false
          }
        };
        $('#" . $elementName . "_container').highcharts(Highcharts.merge(gaugeOptions, {
          series: [{
            data: computePoints(" . $chartValue . "),
            dataLabels: {
              formatter: function () {
                 return null;
              },
            },
        }]
        }));"
        . $computePoints .
      "})(jQuery);
    </script>
    ";
    return $chart;
  }
}

